<?php

namespace App\Http\Controllers;

use App\Models\Basket;
use App\Models\CatMaterial;
use App\Models\City;
use App\Models\Company;
use App\Models\CompanyMaterial;
use App\Models\Image;
use App\Models\MaterialOrder;
use App\Models\Rating;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MaterialController extends Controller
{
    public function Companies(Request $request){
        $rules = [
            'cat_material_id' => 'exists:cat_materials,id'
        ];
        $validator = $this->validator($request->all(),$rules);
        if($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $companies = DB::table('companies')
            ->where('city_id',$request['user']->city_id);
            if  (isset($request['material_id'])){
                $companies = $companies
                ->where('cat_id',$request['material_id']);
            }
            $companies = $companies
            ->orderBy('id','desc')
            ->get();

            $data = [];
            foreach ($companies as $item) {
                $temp['id'] = $item->id;
                $temp['name'] = $item->name;
                $temp['address'] = $item->address;
                $temp['cat'] = CatMaterial::find($item->cat_id);
                $temp['lng'] = $item->lng;
                $temp['lat'] = $item->lat;
                $temp['year'] = $item->year;
                $temp['info'] = $item->info;
                $temp['image'] = "https://doc.louisiana.gov/assets/camaleon_cms/image-not-found-4a963b95bf081c3ea02923dceaeb3f8085e1a654fc54840aac61a57a60903fef.png";
                $temp['phone'] = $item->phone;
                $temp['rating'] = $this->GetRating("companies",$item->id);
                $temp['products'] = DB::table('company_materials')
                    ->join('materials','company_materials.material_id','materials.id')
                    ->where('company_id',$item->id)
                    ->select(
                        'company_materials.id',
                        'company_materials.info',
                        'company_materials.price',
                        'company_materials.count',
                        'materials.name',
                        'materials.image'
                    )
                    ->get();
                $data[] = $temp;
            }

            if (count($data) != 0){
                return $this->Result(200,$data);
            }
            else{
                return $this->Result(404);
            }
        }
    }
    public function Search(Request $request){
        $rules = [
            'text' => 'required'
        ];
        $validator = $this->validator($request->all(),$rules);
        if($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $companies= DB::table('companies')
                ->where('city_id',$request['user']->city_id)
                ->where('name','like',"%$request->text%")
                ->orWhere('info','like',"%$request->text%")
                ->orderBy('id','desc')
                ->get();
            $data = [];
            foreach ($companies as $item) {
                $temp['id'] = $item->id;
                $temp['name'] = $item->name;
                $temp['address'] = $item->address;
                $temp['cat'] = CatMaterial::find($item->cat_id);
                $temp['lng'] = $item->lng;
                $temp['lat'] = $item->lat;
                $temp['year'] = $item->year;
                $temp['info'] = $item->info;
                $temp['phone'] = $item->phone;
                $temp['products'] = DB::table('company_materials')
                    ->join('materials','company_materials.material_id','materials.id')
                    ->where('company_id',$item->id)
                    ->select(
                        'company_materials.id',
                        'company_materials.info',
                        'company_materials.price',
                        'company_materials.count',
                        'materials.name',
                        'materials.image'
                    )
                    ->get();
                $data[] = $temp;
            }

            if (count($data) != 0){
                return $this->Result(200,$data);
            }
            else{
                return $this->Result(404);
            }
        }
    }
    public function GetCompany($id){
        $item = Company::find($id);
        if ($item){
            $temp['id'] = $item->id;
            $temp['name'] = $item->name;
            $temp['address'] = $item->address;
            $temp['cat'] = CatMaterial::find($item->cat_id);
            $temp['lng'] = $item->lng;
            $temp['lat'] = $item->lat;
            $temp['year'] = $item->year;
            $temp['info'] = $item->info;
            $temp['image'] = "https://doc.louisiana.gov/assets/camaleon_cms/image-not-found-4a963b95bf081c3ea02923dceaeb3f8085e1a654fc54840aac61a57a60903fef.png";
            $temp['phone'] = $item->phone;
            $temp['products'] = DB::table('company_materials')
                ->join('materials','company_materials.material_id','materials.id')
                ->where('company_id',$item->id)
                ->select(
                    'company_materials.id',
                    'company_materials.info',
                    'company_materials.price',
                    'company_materials.count',
                    'materials.name',
                    'materials.image'
                )
                ->get();

            $temp['rating'] = $this->GetRating("companies",$item->id);

            return $this->Result(200,$temp);
        }
        else{
            return $this->Result(404);
        }

    }
    public function OrderCreate(Request $request){
        $rules = [
            'cat_material_id' => 'required'
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else{
            $order = new MaterialOrder();
            $order->cat_material_id = $request['cat_material_id'];
            $order->info = $request['info'];
            $order->user_id =  $request['user']->id;
            $order->save();

            $this->uploadImages($request['images'],'material_orders',$order->id);

            return $this->Result(200,$order);
        }
    }
    public function MyOrders(Request $request){
        $orders = MaterialOrder::where('user_id',$request['user']->id)->get();
        $arr = [];
        if (count($orders) != 0){

            foreach ($orders as $order) {
                $temp['id']=$order->id;
                $temp['user_id']=$order->user_id;
                $temp['company_id']=$order->company_id;
                $temp['cat_material_id']=$order->cat_material_id;
                $temp['info']=$order->info;
                $temp['step']=$order->step;
                $temp['images']= Image::where('parent_type',"material_orders")
                    ->where('parent_id',$order->id)
                    ->get();

                $arr[]=$temp;
            }

            return $this->Result(200,$arr);
        }
        else{
            return $this->Result(404);
        }
    }
    public function Basket(Request $request){
        $rules = [
            'company_material_id' => 'required|exists:company_materials,id',
            'count' => 'integer|min:1',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $user = $request['user'];
            $basket = Basket::where('user_id',$user->id)
                ->where('product_type','material')
                ->where('product_id',$request['company_material_id'])
                ->first();

            if  ($basket){
                $basket->product_type = 'material';
                $basket->product_id = $request['company_material_id'];
                $basket->user_id = $user->id;
                $basket->count = ($request['count'] != null) ? $request['count'] : 1;
                $basket->save();

                $basket->save();
            }
            else{
                $basket = new Basket();
                $basket->product_type = 'material';
                $basket->product_id = $request['company_material_id'];
                $basket->user_id = $user->id;
                $basket->count = ($request['count'] != null) ? $request['count'] : 1;
                $basket->save();
            }

            return $this->Result(200);
        }
    }
    public function BasketDelete(Request $request){
        $rules = [
            'company_material_id' => 'required|exists:company_materials,id',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $user = $request['user'];
            $basket = Basket::where('user_id',$user->id)
                ->where('product_type','material')
                ->where('product_id',$request['company_material_id'])
                ->first();
            if  ($basket){
                $basket->delete();
                return $this->Result(200);
            }
            else{
                return $this->Result(404);
            }
        }
    }
    public function BasketAllDelete(Request $request){
        $user = $request['user'];
        Basket::where('user_id',$user->id)
            ->where('product_type','material')
            ->delete();
        return $this->Result(200);
    }
    public function ListBasket(Request $request){
        $user = $request['user'];

        $materials = \DB::table('baskets')
            ->where('user_id',$user->id)
            ->where('product_type','material')
            ->join('materials','baskets.product_id','materials.id')
            ->join('company_materials','baskets.product_id','company_materials.material_id')
            ->join('companies','company_materials.company_id','companies.id')
            ->select(
                'baskets.product_id as id',
                'baskets.count',
                'materials.name',
                'materials.image',
                'company_materials.info',
                'company_materials.price',
                'companies.delivery',
                'companies.name as company_name'
            )
            ->get();
        $amount = 0;
        foreach ($materials as $item) {
            $temp = $item->count * $item->price;
            $amount += $temp;
        }

        if (count($materials) != 0){
            return $this->Result(200,['products'=>$materials,'amount'=>$amount]);

        }
        else{
            return $this->Result(404);
        }
    }
    public function RatingAdd(Request $request){
        $rules = [
            'id' => 'required',
            'ball' => 'required',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $rating = new Rating();
            $rating->parent_type = "companies";
            $rating->parent_id = $request['id'];
            $rating->user_id= $request['user']->id;
            $rating->ball = $request['ball'];
            $rating->comment = $request['comment'];

            $rating->save();
            return $this->Result(200);
        }
    }

    public function Beside( Request $request){
        $user = $request["user"];
        $companies  = Company::scopeDistance($user["lat"],$user['lng'])->limit(10)->get();
        if (count($companies) > 0){
            $data = [];

            foreach ($companies as $item) {
                $temp['id'] = $item->id;
                $temp['name'] = $item->name;
                $temp['address'] = $item->address;
                $temp['cat'] = CatMaterial::find($item->cat_id);
                $temp['lng'] = $item->lng;
                $temp['lat'] = $item->lat;
                $temp['year'] = $item->year;
                $temp['info'] = $item->info;
                $temp['image'] = "https://doc.louisiana.gov/assets/camaleon_cms/image-not-found-4a963b95bf081c3ea02923dceaeb3f8085e1a654fc54840aac61a57a60903fef.png";
                $temp['phone'] = $item->phone;
                $temp['rating'] = $this->GetRating("companies", $item->id);
                $temp['products'] = DB::table('company_materials')
                    ->join('materials', 'company_materials.material_id', 'materials.id')
                    ->where('company_id', $item->id)
                    ->select(
                        'company_materials.id',
                        'company_materials.info',
                        'company_materials.price',
                        'company_materials.count',
                        'materials.name',
                        'materials.image'
                    )
                    ->get();
                $data[] = $temp;
            }

            return $this->Result(200,$data);
        }else{
            return $this->Result(404);
        }
    }

}