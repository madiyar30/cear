<?php

namespace App\Http\Controllers;

use App\Models\CallOrder;
use App\Models\City;
use App\Models\Computes;
use App\Models\Image;
use App\Models\Mark;
use App\Models\Offer;
use App\Models\Rating;
use App\Models\Tech;
use App\Models\TechOrder;
use App\Models\Transport;
use App\Models\User;
use App\PG_Signature;
use App\Push;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function Login(Request $request)
    {
        $rules = [
            'phone' => 'required|exists:users,phone',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {


            $user = User::where('phone',$request['phone'])->first();
            $user->save();

            return $this->User($user);
        }
    }
    public function Auth(Request $request)
    {
        $user = $request['user'];
        return $this->User($user);
    }
    public function Register(Request $request){
        $rules = [
            'phone' => 'required|unique:users,phone|min:10|max:10',
            'email' => 'required|unique:users,email',
            'name' => 'required|max:255',
            'city_id' => 'required',
            'avatar' =>  'mimes:jpeg,jpg,png',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $user = new User();
            $user->id = rand(100000000,999999999);
            $user->phone =(int)$request['phone'];
            $user->email = $request['email'];
            $user->name = $request['name'];
            $user->city_id = $request['city_id'];
            $user->avatar = $this->uploadfile($request['avatar']);
            $user->token = str_random(30);
            $user->role = 'clients';
            $user->save();


            return $this->User($user);
        }
    }
    public function DriverRegister(Request $request){
        $rules = [
            'phone' => 'required|min:10|max:10',
            'name' => 'required|max:255',
            'tech_id' =>  'required|exists:teches,id',
            'transport_id' =>  'required|exists:transports,id',
            'mark_id' =>  'required|exists:marks,id',
            'flight_price' =>  'required',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $user = $request['user'];
            if ($user->is_driver == 0){
                $phone = User::where('phone', $request['phone'])->where('phone', '<>', $request['user']->phone)->first();
                if ($phone){
                    return $this->Result(400,[],'Номер занят');
                }
                $user->phone = $request['phone'];
                $user->name = $request['name'];
                $user->info = $request['info'];
                $user->tech_id = $request['tech_id'];
                $user->transport_id = $request['transport_id'];
                $user->mark_id = $request['mark_id'];
                $user->role = 'drivers';
                $user->flight_price = $request['flight_price'];
                $user->is_driver = 1;
                $user->save();
                $this->uploadImages($request['images'],'drivers',$user->id);
                return $this->User($user);
            }
            else{
                return $this->Result(401,$user,'Ошибка данных');
            }
        }
    }
    public function UserTransition(Request $request){
        $rules = [
            'role' => 'required',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
           if ($request['user']->is_driver == 1){
               if ($request['user']->role != 'workers'){
                   $user = $request['user'];
                   $user->role =  $request['role'];
                   $user->save();

                   return $this->User($user);
               }
               else{
                   return $this->Result(400,[],'Работник не можеть изменить роль');
               }
           }
           else{
               return $this->Result(400,[],'Ошибка is_driver == 1');
           }
        }
    }
    public function User($user){
        if ($user){
            switch ($user->role){
                case 'clients':
                    $data['id'] = $user->id;
                    $data['phone'] = $user->phone;
                    $data['city'] = City::find($user->city_id);
                    $data['email'] = $user->email;
                    $data['avatar'] = $user->avatar;
                    $data['name'] = $user->name;
                    $data['token'] = $user->token;
                    $data['role'] = $user->role;
                    $data['is_driver'] = $user->is_driver;
                    $data['is_brigadier'] = $user->is_brigadier;
                    $data['is_worker'] = $user->is_worker;
                    $data['balance'] = $user->balance;

                    $data['geopositions_io'] = "geopositions_$user->city_id";
                    $data['history_tech_orders'] = [];
                    $orders = TechOrder::where('step',7)->where('client_id',$user->id)->get();
                    foreach ($orders as $order) {
                        $temp = $this->GetTechOrder($order->id);
                        if ($temp['statusCode'] == 200){
                            $data['history_tech_orders'][] = $temp['result'];
                        }
                    }
                    return $this->Result(200,$data);
                case 'drivers':
                    $data['id'] = $user->id;
                    $data['phone'] = $user->phone;
                    $data['city'] = City::find($user->city_id);
                    $data['email'] = $user->email;
                    $data['avatar'] = $user->avatar;
                    $data['name'] = $user->name;
                    $data['token'] = $user->token;
                    $data['balance'] = $user->balance;
                    $data['role'] = $user->role;
                    $data['is_driver'] = $user->is_driver;
                    $data['is_brigadier'] = $user->is_brigadier;
                    $data['is_worker'] = $user->is_worker;
                    $data['tech'] = Tech::find($user->tech_id);
                    $data['rating'] = $this->GetRating("drivers",$user->id);
                    $data['transport'] = Transport::find($user->transport_id);
                    $data['mark'] = Mark::find($user->mark_id);
                    $data['images'] = Image::where("parent_id",$user->id)->where("parent_type","drivers")->pluck("path");

                    $data['info'] = $user->info;
                    $data['lng'] = $user->lng;
                    $data['lat'] = $user->lat;

                    $data['tech_orders_io'] = "tech_orders_$user->city_id";

                    $data['history_tech_orders'] = [];
                    $orders = TechOrder::where('step',7)->where('driver_id',$user->id)->get();
                    foreach ($orders as $order) {
                        $temp = $this->GetTechOrder($order->id);
                        if ($temp['statusCode'] == 200){
                            $arr['history_tech_orders'][] = $temp['result'];
                        }
                    }


                    return $this->Result(200,$data);
                case 'brigadiers':
                    $data['id'] = $user->id;
                    $data['phone'] = $user->phone;
                    $data['city'] = City::find($user->city_id);
                    $data['email'] = $user->email;
                    $data['avatar'] = $user->avatar;
                    $data['name'] = $user->name;
                    $data['token'] = $user->token;
                    $data['balance'] = $user->balance;
                    $data['role'] = $user->role;
                    $data['is_driver'] = $user->is_driver;
                    $data['is_brigadier'] = $user->is_brigadier;
                    $data['is_worker'] = $user->is_worker;
                    $data['tech'] = Tech::find($user->tech_id);
                    $data['transport'] = Transport::find($user->transport_id);
                    $data['mark'] = Mark::find($user->mark_id);
                    $data['workers'] = User::where("role","workers")->where("brigadier_id",$user->id)->get();

                    $data['info'] = $user->info;
                    $data['lng'] = $user->lng;
                    $data['lat'] = $user->lat;

                    $data['tech_orders_io'] = "tech_orders_$user->city_id";

                    $data['history_tech_orders'] = [];
                    $orders = TechOrder::where('step',7)->where('brigadier_id',$user->id)->get();
                    foreach ($orders as $order) {
                        $temp = $this->GetTechOrder($order->id);
                        if ($temp['statusCode'] == 200){
                            $arr['history_tech_orders'][] = $temp['result'];
                        }
                    }


                    return $this->Result(200,$data);
                case 'workers':
                    $data['id'] = $user->id;
                    $data['phone'] = $user->phone;
                    $data['city'] = City::find($user->city_id);
                    $data['email'] = $user->email;
                    $data['avatar'] = $user->avatar;
                    $data['name'] = $user->name;
                    $data['token'] = $user->token;
                    $data['balance'] = $user->balance;
                    $data['role'] = $user->role;
                    $data['brigadier_id'] = $user->brigadier_id;
                    $data['is_driver'] = $user->is_driver;
                    $data['is_brigadier'] = $user->is_brigadier;
                    $data['is_worker'] = $user->is_worker;
                    $data['tech'] = Tech::find($user->tech_id);
                    $data['transport'] = Transport::find($user->transport_id);
                    $data['mark'] = Mark::find($user->mark_id);
                    $data['rating'] = $this->GetRating("drivers",$user->id);
                    $data['images'] = Image::where("parent_id",$user->id)->where("parent_type","drivers")->pluck("path");
                    $data['info'] = $user->info;
                    $data['lng'] = $user->lng;
                    $data['lat'] = $user->lat;
                    $orders = TechOrder::where('step',7)->where('driver_id',$user->id)->get();
                    foreach ($orders as $order) {
                        $temp = $this->GetTechOrder($order->id);
                        if ($temp['statusCode'] == 200){
                            $arr['history_tech_orders'][] = $temp['result'];
                        }
                    }


                    $data['tech_orders_io'] = "tech_orders_$user->city_id";

                    return $this->Result(200,$data);
            }
        }
        else{
            return $this->Result(404);
        }
    }
    public function BrigadierRegister(Request $request){
        $rules = [
            'phone' => 'required|min:10|max:10',
            'name' => 'required|max:255',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $user = $request['user'];
            if ($user->is_driver == 1){
                if ($user->is_brigadier == 0){
                    $phone = User::where('phone', $request['phone'])
                        ->where('phone', '<>', $request['user']
                            ->phone)->first();
                    if ($phone){
                        return $this->Result(400,[],'Номер занят');
                    }
                    $user->phone = $request['phone'];
                    $user->name = $request['name'];
                    $user->role = 'brigadiers';
                    $user->is_brigadier = 1;
                    $user->save();
                    return $this->Result(200);
                }else{
                    return $this->Result(200);
                }
            }
            else{
                return $this->Result(440,[],'user->is_driver == 1');
            }
        }
    }
    public function WorkerAdd(Request $request){
        $rules = [
            'id' => 'required',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }else{
            $worker = User::where("is_driver",1)
                ->where("id",$request['id'])
                ->where("brigadier_id",null)
                ->first();
            if ($worker){
                $worker->brigadier_id= $request['user']->id;
                $worker->role = 'workers';
                $worker->save();


                $push= new Push();
                $push->Worker($worker->id);

                return $this->Result(200);
            }else{
                return $this->Result(404);
            }
        }
    }
    public function WorkerRemove(Request $request){
        $rules = [
            'id' => 'required',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }else{
            $worker = User::where("role","workers")
                ->where("id",$request['id'])
                ->where("brigadier_id",$request['user']->id)
                ->first();
            if ($worker){
                $worker->brigadier_id= null;
                $worker->role = 'drivers';
                $worker->save();

                return $this->Result(200);
            }else{
                return $this->Result(404);
            }
        }
    }
    public function GetTechOrder($id){
        $order = TechOrder::find($id);
        $temp['id'] = $order->id;
        $temp['client'] = User::find($order->client_id);
        $temp['driver'] = User::find($order->driver_id);
        $temp['brigadier_id'] = User::find($order->brigadier_id);
        $temp['address'] = $order->address;
        $temp['lng'] = $order->lng;
        $temp['lat'] = $order->lat;
        $temp['tech'] = Tech::find($order->tech_id);
        $temp['transport'] = Transport::find($order->transport_id);
        $temp['computes'] = Computes::find($order->compute_id);
        $temp['commission'] = Computes::find($order->commission_id);
        $temp['price'] = $order->price;
        $temp['comment'] = $order->comment;
        $temp['start_date'] = $order->start_date;
        $temp['step'] = $order->step;
        $temp['distance_traveled'] = $order->distance_traveled;
        $temp['total_amount'] = $order->total_amount;
        $temp['start_work'] = $order->start_work;
        $temp['end_work'] = $order->end_work;
        $temp['hour_work'] = $order->hour_work;
        $temp['created_at'] = $order->created_at;

        $temp['images'] = Image::where('parent_type','tech_orders')->where('parent_id',$order['id'])->pluck('path');
        $temp['offers'] =   Offer::join('users','offers.user_id','users.id')
        ->where('order_id',$order->id)
        ->select('users.*')
        ->get();




        $result['statusCode'] = 200;
        $result['message'] = "success";
        $result['result'] = $temp;

        return $result;
    }
    public function AddBalance(Request $request)
    {
        /*
            Идентификатор проекта: 501400
            Секретный ключ для приема платежей: pBPvVqQtKXBfSZoP
            Секретный ключ для выплат клиентам: 8kzwOeOGOoQvUTUi
         */
        $rules = [
            'balance' => 'required',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $user = $request['user'];

            $arrReq = [
                'pg_merchant_id' => '501400',
                'pg_amount' => $request['balance'],
                'pg_description' => 'CearTrance payment',
                'pg_order_id' => $user->id,
                'pg_salt' => mt_rand(21, 43433),
                'pg_result_url' => route('AddBalanceResult')
            ];
            $arrReq['pg_sig'] = PG_Signature::make('payment.php', $arrReq, 'pBPvVqQtKXBfSZoP');

            $query = http_build_query($arrReq);


            return $this->Result(200,['url'=>'https://www.paybox.kz/payment.php?'.$query]);




        }
    }
    public function AddBalanceResult(Request $request)
    {
        Storage::append('paybox.log',$request);
        if($request['pg_result']) {
            $order= User::find($request['pg_order_id']);
            $order->balance =  $order->balance + $request['pg_amount'];
            $order->save();
            $arrReq = [
                'pg_merchant_id' => 501400,
                'pg_salt' => mt_rand(21, 43433)
            ];
            $pg_sig = PG_Signature::make('payment.php', $arrReq, 'pBPvVqQtKXBfSZoP');
            $pg_salt = str_random(10);

            $xmlResponce = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<response>
<pg_salt>$pg_salt</pg_salt>
<pg_status>ok</pg_status>
<pg_description>Товар передан покупателю</pg_description>
<pg_sig>$pg_sig</pg_sig>
</response>
XML;

            return $xmlResponce;
        }
    }
    public function Call(Request $request){
        $rules = [
            'name' => 'required',
            'phone' => 'required|min:10|max:10',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $call = new CallOrder();

            $call->name = $request['name'];
            $call->phone = $request['phone'];
            $call->save();

            return $this->Result(200);
        }
    }

    public function RatingAdd(Request $request){
        $rules = [
            'driver_id' => 'required',
            'ball' => 'required',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $rating = new Rating();
            $rating->parent_type = "drivers";
            $rating->parent_id = $request['driver_id'];
            $rating->user_id= $request['user']->id;
            $rating->ball = $request['ball'];
            $rating->comment = $request['comment'];

            $rating->save();
            return $this->Result(200);
        }
    }
}

