<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Phone;
use App\Models\Point;
use App\Models\Shipping;
use App\Models\ShippingCargo;
use App\Models\ShippingTransport;
use App\Models\Transport;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShippingController extends Controller
{
    public function Shippings(Request $request){
        $rules = [
            'page' => 'required',
            'shipping_type' => 'required',
        ];
        $validator = $this->validator($request->all(),$rules);
        if($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $count =  DB::table('shippings')
                ->where("shipping_type",$request['shipping_type'])
                ->count();

            $limit = 20;
            $offset = $limit * $request['page'];
            $pages = (int)ceil($count/$limit) - 1;

            $shippings= DB::table('shippings')
                ->where("shipping_type",$request['shipping_type'])
                ->limit($limit)
                ->offset($offset)
                ->orderBy('id','desc')
                ->get();
            $data = [];
            foreach ($shippings as $item) {
                $temp = $this->GetShipping($item->id);
                if ($temp['statusCode'] == 200){
                    $data[] = $temp['result'];
                }
            }

            if (count($data) != 0){
                $result['count_pages'] = $pages;
                $result['count_data'] = $count;
                $result['offset'] = $offset;
                $result['limit'] = $limit;
                $result['current_page'] = (int)$request['page'];
                $result['data'] = $data;

                return $this->Result(200,$result);

            }
            else{
                return $this->Result(404);
            }
        }
    }
    public function CreateShippingCargo(Request $request){
        $rules = [
            'weight' => 'required',
            'size' => 'required',
            'transport_id' => 'required|exists:transports,id',
            'count_transport' => 'required',
            'name' => 'required',
            'company_name' => 'required',
            'email' => 'required',
            'info' => 'required',
            'phones' => 'required|array|min:1',
            'points' => 'required|array|min:1'
        ];
        $validator = $this->validator($request->all(),$rules);
        if($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $shipping = new Shipping();
            $shipping->user_id = $request['user']->id;
            $shipping->shipping_type = 'cargo';
            $shipping->save();

            foreach ($request['phones'] as $item){
                $phone = new Phone();
                $phone->phone = $item;
                $phone->parent_id = $shipping->id;
                $phone->parent_type = 'shipping';
                $phone->save();
            }

            foreach ($request['points'] as $item){
                $point = new Point();
                $point->shipping_id = $shipping->id;
                $point->type = $item['type'];
                $point->city_id = $item['city_id'];
                $point->save();
            }


            $cargo = new ShippingCargo();
                $cargo->shipping_id = $shipping->id;
                $cargo->weight = $request['weight'];
                $cargo->size = $request['size'];
                $cargo->transport_id = $request['transport_id'];
                $cargo->count_transport = $request['count_transport'];
                $cargo->name = $request['name'];
                $cargo->company_name = $request['company_name'];
                $cargo->email = $request['email'];
                $cargo->info = $request['info'];
            $cargo->save();

            return $this->Result(200);
        }
    }
    public function CreateShippingTransport(Request $request){
        $rules = [
            'weight' => 'required',
            'size' => 'required',
            'transport_id' => 'required|exists:transports,id',
            'count_transport' => 'required',
            'name' => 'required',
            'company_name' => 'required',
            'email' => 'required',
            'info' => 'required',
            'phones' => 'required|array|min:1',
            'points' => 'required|array|min:1'
        ];
        $validator = $this->validator($request->all(),$rules);
        if($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $shipping = new Shipping();
            $shipping->user_id = $request['user']->id;
            $shipping->shipping_type = 'transport';
            $shipping->loading_date = Carbon::parse($request['loading_date']);
            $shipping->unloading_date = Carbon::parse($request['unloading_date']);
            $shipping->shipping_type = 'transport';
            $shipping->save();

            foreach ($request['phones'] as $item){
                $phone = new Phone();
                $phone->phone = $item;
                $phone->parent_id = $shipping->id;
                $phone->parent_type = 'shipping';
                $phone->save();
            }

            foreach ($request['points'] as $item){
                $point = new Point();
                $point->shipping_id = $shipping->id;
                $point->type = $item['type'];
                $point->city_id = $item['city_id'];
                $point->save();
            }


            $transport = new ShippingTransport();
               $transport->shipping_id = $shipping->id;
               $transport->weight = $request['weight'];
               $transport->size = $request['size'];
               $transport->transport_id = $request['transport_id'];
               $transport->count_transport = $request['count_transport'];
               $transport->name = $request['name'];
               $transport->company_name = $request['company_name'];
               $transport->email = $request['email'];
               $transport->info = $request['info'];
            $transport->save();

            return $this->Result(200);
        }
    }
    public function GetShipping($id){
        $shipping = Shipping::find($id);
        if ($shipping){
            $data['id'] = $shipping->id;
            $data['shipping_type'] = $shipping->shipping_type;
            $data['user'] = User::find($shipping->user_id);
            $data['loading_date'] = $shipping->loading_date;
            $data['unloading_date'] = $shipping->unloading_date;
            $data['user'] = User::find($shipping->user_id);

            $data['phones'] = Phone::where('parent_type','shipping')->where('parent_id',$shipping->id)->pluck('phone');
            $data['points'] = Point::join("cities","cities.id","points.city_id")
                    ->where('shipping_id',$shipping->id)
                    ->select('points.id',"cities.id as city_id","cities.name","points.type")
                    ->get();



            if ($shipping->shipping_type == 'cargo'){
                $cargo = ShippingCargo::where('shipping_id',$shipping->id)->orderBy('id','desc')->first();
                if ($cargo){
                    $data['weight'] = $cargo->weight;
                    $data['size'] = $cargo->size;
                    $data['transport'] = Transport::find($cargo->transport_id);
                    $data['count_transport'] = $cargo->	count_transport;
                    $data['name'] = $cargo->name;
                    $data['company_name'] = $cargo->company_name;
                    $data['email'] = $cargo->email;
                    $data['info'] = $cargo->info;

                    $result['statusCode'] = 200;
                    $result['message'] = 'Успешно';
                    $result['result'] = $data;
                }
                else{
                    $result['statusCode'] = 404;
                    $result['message'] = 'Не найдено';
                    $result['result'] = [];
                }
            }
            else{
                $transport =  ShippingTransport::where('shipping_id',$shipping->id)->orderBy('id','desc')->first();

                if ($transport){
                    $data['weight'] = $transport->weight;
                    $data['size'] = $transport->size;
                    $data['transport'] = Transport::find($transport->transport_id);
                    $data['count_transport'] = $transport->	count_transport;
                    $data['name'] = $transport->name;
                    $data['company_name'] = $transport->company_name;
                    $data['email'] = $transport->email;
                    $data['info'] = $transport->info;

                    $result['statusCode'] = 200;
                    $result['message'] = 'Успешно';
                    $result['result'] = $data;
                }
                else{
                    $result['statusCode'] = 404;
                    $result['message'] = 'Не найдено';
                    $result['result'] = [];
                }
            }
        }
        else{
            $result['statusCode'] = 404;
            $result['message'] = 'Не найдено';
            $result['result'] = [];
        }

        return $result;
    }
    public function Count(){

        $t = Shipping::where('shipping_type',"transport")->count();
        $c = Shipping::where('shipping_type',"cargo")->count();

        $result['statusCode'] = 200;
        $result['message'] = 'Успешно';
        $result['result']["transport"] = $t;
        $result['result']["cargo"] = $c;

        return $result;
    }



}