<?php

namespace App\Http\Controllers;

use App\Models\Basket;
use App\Models\City;
use App\Models\Image;
use App\Models\PartCat;
use App\Models\PartOrder;
use App\Models\Rating;
use App\Models\User;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PartController extends Controller
{
    public function Shops(Request $request){
        $user = $request['user'];
        $shops = DB::table('shops')
            ->where('city_id',$user->city_id)
            ->orderBy('id','desc')
            ->get();
        $data = [];
        foreach ($shops as $item) {
            $temp['id'] = $item->id;
            $temp['name'] = $item->name;
            $temp['address'] = $item->address;
            $temp['part_cat'] = PartCat::find($item->part_cat_id);
            $temp['lng'] = $item->lng;
            $temp['lat'] = $item->lat;
            $temp['info'] = $item->info;
            $temp['phone'] = $item->phone;
            $temp['image'] = $item->image;
            $temp['rating'] = $this->GetRating("shops",$item->id);
            $temp['parts'] = DB::table('parts')
                ->where('shop_id',$item->id)
                ->get();
            $data[] = $temp;
        }

        if (count($data) != 0){
            return $this->Result(200,$data);
        }
        else{
            return $this->Result(404);
        }
    }
    public function GetShop($id){
        $item = Shop::find($id);
        if ($item){
            $temp['id'] = $item->id;
            $temp['name'] = $item->name;
            $temp['address'] = $item->address;
            $temp['part_cat'] = PartCat::find($item->part_cat_id);
            $temp['lng'] = $item->lng;
            $temp['lat'] = $item->lat;
            $temp['year'] = $item->year;
            $temp['info'] = $item->info;
            $temp['phone'] = $item->phone;
            $temp['image'] = $item->image;
            $temp['rating'] = $this->GetRating("shops",$item->id);
            $temp['parts'] = DB::table('parts')
                ->where('shop_id',$item->id)
                ->get();
            $data[] = $temp;

            return $this->Result(200,$temp);
        }
        else{
            return $this->Result(404);
        }

    }
    public function OrderCreate(Request $request){
        $rules = [
            'part_cat_id' => 'required'
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else{
            $order = new PartOrder();
            $order->part_cat_id = $request['part_cat_id'];
            $order->info = $request['info'];
            $order->user_id =  $request['user']->id;
            $order->save();

            $this->uploadImages($request['images'],'part_orders',$order->id);

            return $this->Result(200,$order);
        }
    }
    public function MyOrders(Request $request){
        $orders = PartOrder::where('user_id',$request['user']->id)->get();
        if (count($orders) != 0){
            $arr = [];
            foreach ($orders as $order) {
                $temp['id']=$order->id;
                $temp['user_id']=$order->user_id;
                $temp['shop_id']=$order->shop_id;
                $temp['part_cat_id']=$order->part_cat_id;
                $temp['info']=$order->info;
                $temp['step']=$order->step;
                $temp['images']= Image::where('parent_type',"part_orders")
                    ->where('parent_id',$order->id)
                    ->get();

                $arr[]=$temp;
            }

            return $this->Result(200,$arr);
        }
        else{
            return $this->Result(404);
        }
    }
    public function Basket(Request $request){
        $rules = [
            'part_id' => 'required|exists:parts,id',
            'count' => 'integer|min:1',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $user = $request['user'];
            $basket = Basket::where('user_id',$user->id)
                ->where('product_type','part')
                ->where('product_id',$request['part_id'])
                ->first();

            if  ($basket){
                $basket->product_type = 'part';
                $basket->product_id = $request['part_id'];
                $basket->user_id = $user->id;
                $basket->count = ($request['count'] != null) ? $request['count'] : 1;
                $basket->save();
            }
            else{
                $basket = new Basket();
                $basket->product_type = 'part';
                $basket->product_id = $request['part_id'];
                $basket->user_id = $user->id;
                $basket->count = ($request['count'] != null) ? $request['count'] : 1;
                $basket->save();
            }

            return $this->Result(200);
        }
    }
    public function BasketDelete(Request $request){
        $rules = [
            'part_id' => 'required|exists:parts,id',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $user = $request['user'];
            $basket = Basket::where('user_id',$user->id)
                ->where('product_type','part')
                ->where('product_id',$request['part_id'])
                ->first();

            if  ($basket){
                $basket->delete();
                return $this->Result(200);
            }
            else{
                return $this->Result(404);
            }
        }
    }
    public function BasketAllDelete(Request $request){
        $user = $request['user'];
        Basket::where('user_id',$user->id)
            ->where('product_type','part')
            ->delete();
        return $this->Result(200);
    }
    public function ListBasket(Request $request){
        $user = $request['user'];

        $materials = \DB::table('baskets')
            ->where('user_id',$user->id)
            ->where('product_type','part')
            ->join('parts','baskets.product_id','parts.id')
            ->join('shops','parts.shop_id','shops.id')
            ->select(
                'baskets.product_id as id',
                'baskets.count',
                'parts.name',
                'parts.image',
                'parts.info',
                'parts.price',
                'shops.name as shop_name'
            )
            ->get();
        $amount = 0;
        $data = [];
        foreach ($materials as $item) {
            $temp = $item->count * $item->price;
            $amount += $temp;

            $temp2['id'] = $item->id;
            $temp2['count'] = $item->count;
            $temp2['name'] = $item->name;
            $temp2['image'] = $item->image;
            $temp2['info'] = $item->info;
            $temp2['price'] = $item->price;
            $temp2['delivery'] = 0;
            $temp2['shop_name'] =$item->shop_name;

            $data[] = $temp2;
        }
        if (count($materials) != 0){
            return $this->Result(200,['products'=>$data,'amount'=>$amount]);

        }
        else{
            return $this->Result(404);
        }

    }
    public function RatingAdd(Request $request){
        $rules = [
            'id' => 'required',
            'ball' => 'required',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $rating = new Rating();
            $rating->parent_type = "shops";
            $rating->parent_id = $request['id'];
            $rating->user_id= $request['user']->id;
            $rating->comment = $request['comment'];
            $rating->ball = $request['ball'];

            $rating->save();
            return $this->Result(200);
        }
    }




    public function Beside( Request $request){
        $user = $request["user"];
        $shops  = Shop::scopeDistance($user["lat"],$user['lng'])->limit(10)->get();
        if (count($shops) > 0){
            $data = [];
            foreach ($shops as $item) {
                $temp['id'] = $item->id;
                $temp['name'] = $item->name;
                $temp['address'] = $item->address;
                $temp['part_cat'] = PartCat::find($item->part_cat_id);
                $temp['lng'] = $item->lng;
                $temp['lat'] = $item->lat;
                $temp['info'] = $item->info;
                $temp['phone'] = $item->phone;
                $temp['image'] = $item->image;
                $temp['rating'] = $this->GetRating("shops",$item->id);
                $temp['parts'] = DB::table('parts')
                    ->where('shop_id',$item->id)
                    ->get();
                $data[] = $temp;
            }

            return $this->Result(200,$data);
        }else{
            return $this->Result(404);
        }
    }
}

