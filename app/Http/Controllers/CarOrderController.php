<?php

namespace App\Http\Controllers;

use App\Models\CarOrder;
use App\Models\CarOrdersReview;
use App\Models\CatCarOrder;
use App\Models\City;

use App\Models\Image;
use App\Models\Mark;
use App\Models\Tech;
use App\Models\Transport;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class CarOrderController extends Controller
{
    public function CreateCarOrder(Request $request){
//        Storage::append('logging.log',json_encode($request->all()));
        $rules = [
            'tech_id' => 'required|exists:teches,id',
            'transport_id' => 'required|exists:transports,id',
            'mark_id' => 'required|exists:marks,id',
            'year' => 'required|max:255',
            'name' => 'required|max:255',
            'phone' => 'required|min:10|max:10',
            'price' => 'required',
            'address' => 'required',
            'info' => 'required',
            'cat_id' => 'required',
            'images' => 'required|array'
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {

            return $this->Result(400,$validator->errors());
        }
        else{
            $order = new CarOrder();
            $order->cat_id = $request['cat_id'];
            $order->tech_id = $request['tech_id'];
            $order->transport_id = $request['transport_id'];
            $order->mark_id = $request['mark_id'];
            $order->year = $request['year'];
            $order->name = $request['name'];
            $order->phone = $request['phone'];
            $order->price = $request['price'];
            $order->address = $request['address'];
            $order->info = $request['info'];
            $order->user_id = $request['user']->id;
            $order->save();

            $this->uploadImages($request['images'],"car_orders",$order->id);

            return $this->Result(200,$this->GetCarOrder($order->id)['result']);
        }
    }
    public function MyCarOrders(Request $request){
        $orders = CarOrder::where('user_id',$request['user']->id)->get();
        $data = [];

        foreach ($orders as $order) {
            $temp = $this->GetCarOrder($order->id);

            if ($temp['statusCode'] == 200){
                $data[] = $temp['result'];
            }
        }
        if (count($data) != 0){
            return $this->Result(200,$data);
        }
        else{
            return $this->Result(404);
        }
    }
    public function CarOrders(Request $request){
        $rules = [
            'page' => 'required',
            'cat_id' => 'required',
        ];
        $validator = $this->validator($request->all(),$rules);
        if($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $count = DB::table('car_orders')
                ->where("cat_id",$request['cat_id'])
                ->count();
            $limit = 10;
            $offset = $limit * $request['page'];
            $pages = (int)ceil($count/$limit) - 1;

            $orders= DB::table('car_orders')
                ->where("cat_id",$request['cat_id'])
                ->limit($limit)
                ->offset($offset)
                ->orderBy('id','desc')
                ->get();
            $data = [];

            foreach ($orders as $order) {
                $temp = $this->GetCarOrder($order->id);

                if ($temp['statusCode'] == 200){
                    $data[] = $temp['result'];
                }
            }

            if (count($data) != 0){
                $result['count_pages'] = $pages;
                $result['count_data'] = $count;
                $result['offset'] = $offset;
                $result['limit'] = $limit;
                $result['current_page'] = (int)$request['page'];
                $result['data'] = $data;

                return $this->Result(200,$result);

            }
            else{
              return $this->Result(404);
            }
        }
    }
    public function GetCarOrder($id){
        $car = CarOrder::find($id);
        if ($car){
            $data['id'] = $car->id;
            $data['cat'] = CatCarOrder::find($car->cat_id);
            $data['tech_id']= Tech::find($car->tech_id);
            $data['transport_id']= Transport::find($car->transport_id);
            $data['mark_id']= Mark::find($car->mark_id);
            $data['year']= $car->year;
            $data['name']= $car->name;
            $data['phone']= $car->phone;
            $data['price']= $car->price;
            $data['address']= $car->address;
            $data['info']= $car->info;
            $data['user']= User::find($car->user_id);
            $data['images']=Image::where('parent_type',"car_orders")->where("parent_id",$car->id)->get();

            $data['reviews'] = CarOrdersReview::where("car_order_id",$car->id)
                ->join('users','users.id','car_orders_reviews.user_id')
                ->select(
                    "users.name" ,
                    "users.phone",
                    "users.avatar",
                    "car_orders_reviews.comment"
                )
                ->get();



            $result['statusCode'] = 200;
            $result['message'] = 'Успешно';
            $result['result'] = $data;
        }
        else{
            $result['statusCode'] = 404;
            $result['message'] = 'Не найдено';
            $result['result'] = [];
        }

        return $result;
    }
    public function ReviewsCreate(Request $request)
    {
        $rules = [
            'car_order_id' => 'required',
            'comment' => 'required',
        ];
        $validator = $this->validator($request->all(), $rules);
        if ($validator->fails()) {

            return $this->Result(400, $validator->errors());
        }
        else{
            $r = new CarOrdersReview();
            $r->user_id = $request['user']->id;
            $r->comment = $request['comment'];
            $r->car_order_id = $request['car_order_id'];
            $r->save();

            return $this->Result(200);
        }
    }
}