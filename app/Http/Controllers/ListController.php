<?php

namespace App\Http\Controllers;

use App\lol;
use App\Models\CatCarOrder;
use App\Models\CatMaterial;
use App\Models\City;
use App\Models\Mark;
use App\Models\PartCat;
use App\Models\Rate;
use App\Models\Tech;
use App\Models\Transport;
use App\Models\User;
use App\Models\Commissions;
use App\Models\Computes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ListController extends Controller
{
    public function Cities(){

        $cities = City::all();
        if (count($cities) != 0){
            return $this->Result(200,$cities);
        }
        else{
            return $this->Result(404,[]);
        }
    }
    public function Marks(){
        $marks = Mark::all();
        if (count($marks) != 0){
            return $this->Result(200,$marks);
        }
        else{
            return $this->Result(404,[]);
        }
    }
    public function Techs(){
        $techs = Tech::all();
        if (count($techs) != 0){
            return $this->Result(200,$techs);
        }
        else{
            return $this->Result(404,[]);
        }
    }
    public function Transports(Request $request){
        $rules = [
            'tech_id' => 'required|exists:teches,id',
        ];
        $validator = $this->validator($request->all(), $rules);

        if ($validator->fails()) {
            return $this->Result(400,$validator->errors());
        }
        else {
            $transports = Transport::where('tech_id',$request['tech_id'])->get();
            if (count($transports) != 0){
                return $this->Result(200,$transports);
            }
            else{
                return $this->Result(404,[]);
            }
        }
    }
    public function Rates(Request $request){
        $rates = Rate::all();
        if (count($rates) != 0){
            return $this->Result(200,$rates);
        }
        else{
            return $this->Result(404,[]);
        }
    }
    public function Сomputes(Request $request){
        $computes = Computes::all();
        if (count($computes) != 0){
            return $this->Result(200,$computes);
        }
        else{
            return $this->Result(404,[]);
        }
    }
    public function Commissions(Request $request){
        $commissions = Commissions::all();
        if (count($commissions) != 0){
            return $this->Result(200,$commissions);
        }
        else{
            return $this->Result(404,[]);
        }
    }
    public function CatMaterials(Request $request){
        $cats = CatMaterial::all();
        if (count($cats) != 0){
            return $this->Result(200,$cats);
        }
        else{
            return $this->Result(404,[]);
        }
    }
    public function CatParts(Request $request){
        $cats = PartCat::all();
        if (count($cats) != 0){
            return $this->Result(200,$cats);
        }
        else{
            return $this->Result(404,[]);
        }
    }
    public function CatCarOrders(Request $request){
        $cats = CatCarOrder::all();
        if (count($cats) != 0){
            return $this->Result(200,$cats);
        }
        else{
            return $this->Result(404,[]);
        }
    }

}
