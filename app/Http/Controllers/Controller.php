<?php

namespace App\Http\Controllers;

use App\Models\Rating;
use http\Env\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Support\Facades\DB;
use Validator;
use File;
use App\Models\Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function validator($errors,$rules) {
        return Validator::make($errors,$rules);
    }
    public function uploadFile($file,$dir = 'uploads'){
        if (isset($file)){
            $file_type = File::extension($file->getClientOriginalName());
            $file_name = time().str_random(5).'.'.$file_type;
            $file->move($dir, $file_name);
            return $dir.'/'.$file_name;
        }
    }
    public function uploadImages($images,$parent_type,$parent_id){
        if ($images){
            if (count($images) != 0){
                foreach ($images as $img) {
                    $image = new Image();
                    $image->parent_type = $parent_type;
                    $image->parent_id = $parent_id;
                    $image->path = $this->uploadFile($img);
                    $image->save();
                }
            }
        }
    }
    public function deletefile($path){
        if (File::exists($path)) {
            File::delete($path);
            return true;
        }
        else{
            return false;
        }
    }

    public function Result($statusCode,$data = array(),$message = null){

        switch ($statusCode){
            case 200:
                $result['statusCode'] = $statusCode;
                $result['message'] = 'Успешно';
                $result['result'] = $data;
                break;

            case 404:
                $result['statusCode'] = $statusCode;
                $result['message'] = 'Не найдено';
                $result['result'] = $data;
                break;
            case 401:
                $result['statusCode'] = 401;
                $result['message'] = 'не авторизован';
                $result['result'] = [];
                break;
            case 400:
                $result['statusCode'] = 400;
                $result['message'] = 'Ошибка данных';
                $result['result'] = $data;
                break;
            default:
                $result['statusCode'] = $statusCode;
                $result['message'] = $message;
                $result['result'] = $data;
                break;
        }

        return response()->json($result, $result['statusCode']);

    }
    public function GetRating($type,$id){
        $rating = Rating::where("parent_type",$type)->where("parent_id",$id)->avg("ball");
        $ratings = Rating::where("parent_type",$type)
            ->where("parent_id",$id)
            ->join("users","users.id","ratings.user_id")
            ->select(
                "ratings.ball",
                "users.name",
                "users.avatar",
                "ratings.created_at",
                "ratings.comment"
            )
            ->get();

        $result['ball'] = round($rating);
        $result['reviews'] = $ratings;


        return $result;
    }



}
