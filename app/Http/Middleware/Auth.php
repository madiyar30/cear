<?php

namespace App\Http\Middleware;

use App\Models\Master;
use App\Models\User;
use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->headers->has('auth')){
            $user = User::where('token',$request->header('auth'))->first();
            if ($user){
                $request['user'] = $user;
                return $next($request);
            }
            else{
                $result['statusCode'] = 404;
                $result['message'] = 'Пользватель не найден';
                $result['result'] = [];
            }
        }
        else{
            $result['statusCode'] = 401;
            $result['message'] = 'не авторизован';
            $result['result'] = [];
        }

        return response()->json($result, $result['statusCode']);
    }
}
