<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartCat extends Model
{
    protected $hidden = ['created_at','updated_at'];
}
