<?php

namespace App;
use App\Models\Offer;
use GuzzleHttp\Client;
use DB;
use GuzzleHttp\Psr7\Request;

class Push{

    protected $key = 'key=AAAA4usaBoQ:APA91bHzPGD_QtRFV_mZLwB8YzJhD_fqBtansaNm3L7hgfVCk350TRgSKJX0pPXHaJOcl8Nhnulk4pP6AzXZ2v5fkEXeGtfg1EHliLEHbnR9V_nfPoa-pNlAspmWX9K4Ew-B0_3eZ2Hs';

    public function Worker($user_id){
        $client = new Client;
        $client->request('POST','https://fcm.googleapis.com/fcm/send',[
                'headers' => [
                    'Authorization' => $this->key,
                    'Content-Type'     => 'application/json',
                ],
                'json' =>[
                    "to" => "/topics/$user_id"."_worker_a",
                    "data" => [
                        "body" => "Вы стали Работником",
                        "title" => "CearTrans",
                    ]
                ]]
        );
        $client->request('POST','https://fcm.googleapis.com/fcm/send',[
                'headers' => [
                    'Authorization' => $this->key,
                    'Content-Type'     => 'application/json',
                ],
                'json' =>[
                    "to" => "/topics/$user_id"."_worker",
                    "notification" => [
                        "body" => "Вы стали Работником",
                        "title" => "CearTrans",
                        "sound" => "default"
                    ]
                ]]
        );
    }

    public function OfferCancel($driver_token,$order_type,$order_id,$offer_id,$client_id){
        $order_type = str_replace('_orders','',$order_type);
        $offer = Offer::find($offer_id);
        $client = new Client;
        $client->request('POST','https://fcm.googleapis.com/fcm/send',[
                'headers' => [
                    'Authorization' => $this->key,
                    'Content-Type'     => 'application/json',
                ],
                'json' =>[
                    "to" => "/topics/$driver_token"."a",
                    "data" => [
                        "body" => "Вашу цену $offer->price тенге Отказали, попробуйте еще раз",
                        "title" => "Easy",
                        'order_type'=> $order_type,
                        'order_id'=> $order_id,
                        'offer_id'=> $offer_id,
                        'client_id'=> $client_id,
                        'step'=> 4
                    ],
                ]]
        );
        $client->request('POST','https://fcm.googleapis.com/fcm/send',[
                'headers' => [
                    'Authorization' => $this->key,
                    'Content-Type'     => 'application/json',
                ],
                'json' =>[
                    "to" => "/topics/$driver_token",
                    "notification" => [
                        "body" => "Вашу цену $offer->price тенге Отказали, попробуйте еще раз",
                        "title" => "Easy",
                        'order_type'=> $order_type,
                        'order_id'=> $order_id,
                        'offer_id'=> $offer_id,
                        'client_id'=> $client_id,
                        'step'=> 4,
                        "sound" => "default"
                    ]
                ]]
        );

    }
    //Клиент принимает отклик
    public function Access($driver_token,$order_type,$order_id,$offer_id,$client_id){
        $order_type = str_replace('_orders','',$order_type);
        $client = new Client;
        $client->request('POST','https://fcm.googleapis.com/fcm/send',[
                'headers' => [
                    'Authorization' => $this->key,
                    'Content-Type'     => 'application/json',
                ],
                'json' =>[
                    "to" => "/topics/$driver_token"."a",
                    "data" => [
                        "body" => "Клиент принял ваше предложение",
                        "title" => "Easy",
                        'order_type'=> $order_type,
                        'order_id'=> $order_id,
                        'offer_id'=> $offer_id,
                        'client_id'=> $client_id,
                        'step'=> 2
                    ],
                ]]
        );
        $client->request('POST','https://fcm.googleapis.com/fcm/send',[
                'headers' => [
                    'Authorization' => $this->key,
                    'Content-Type'     => 'application/json',
                ],
                'json' =>[
                    "to" => "/topics/$driver_token",
                    "notification" => [
                        "body" => "Клиент принял ваше предложение",
                        "title" => "Easy",
                        'order_type'=> $order_type,
                        'order_id'=> $order_id,
                        'offer_id'=> $offer_id,
                        'client_id'=> $client_id,
                        'step'=> 2,
                        "sound" => "default"
                    ]
                ]]
        );
    }

    public function OrderCancel($token,$order){
        $client = new Client;
        $client->request('POST','https://fcm.googleapis.com/fcm/send',[
                'headers' => [
                    'Authorization' => $this->key,
                    'Content-Type'     => 'application/json',
                ],
                'json' =>[
                    "to" => "/topics/$token"."a",
                    "data" => [
                        "body" => "Текущий заказ на сумму $order->price отменен",
                        "title" => "Easy",
                        'order_type'=> $order->type,
                        'order_id'=> $order->id,
                        'offer_id'=> 0,
                        'step'=> 5
                    ],
                ]]
        );
        $client->request('POST','https://fcm.googleapis.com/fcm/send',[
                'headers' => [
                    'Authorization' => $this->key,
                    'Content-Type'     => 'application/json',
                ],
                'json' =>[
                    "to" => "/topics/$token",
                    "notification" => [
                        "body" => "Текущий заказ на сумму $order->price отменен",
                        "title" => "Easy",
                        'order_type'=> $order->type,
                        'order_id'=> $order->id,
                        'offer_id'=> 0,
                        'step'=> 5,
                        "sound" => "default"
                    ]
                ]]
        );
    }

    public function End($client_token,$order_type,$order_id,$driver_id){
        $order_type = str_replace('_orders','',$order_type);
        $client = new Client;
        $client->request('POST','https://fcm.googleapis.com/fcm/send',[
                'headers' => [
                    'Authorization' => $this->key,
                    'Content-Type'     => 'application/json',
                ],
                'json' =>[
                    "to" => "/topics/$client_token"."a",
                    "data" => [
                        "body" => "Мастер закончил заявку",
                        "title" => "Easy",
                        'order_type'=> $order_type,
                        'order_id'=> $order_id,
                        'driver_id'=> $driver_id,
                        'step'=> 3
                    ],
                ]]
        );
        $client->request('POST','https://fcm.googleapis.com/fcm/send',[
                'headers' => [
                    'Authorization' => $this->key,
                    'Content-Type'     => 'application/json',
                ],
                'json' =>[
                    "to" => "/topics/$client_token",
                    "notification" => [
                        "body" => "Мастер закончил заявку",
                        "title" => "Easy",
                        'order_type'=> $order_type,
                        'order_id'=> $order_id,
                        'driver_id'=> $driver_id,
                        'step'=> 3,
                        "sound" => "default"
                    ]
                ]]
        );
    }

    public function Invitation($client_token,$group_id){
        $client = new Client;
        $client->request('POST','https://fcm.googleapis.com/fcm/send',[
                'headers' => [
                    'Authorization' => $this->key,
                    'Content-Type'     => 'application/json',
                ],
                'json' =>[
                    "to" => "/topics/$client_token"."a",
                    "data" => [
                        "body" => "Приглашения",
                        "title" => "Easy",
                        'order_type'=> "invite",
                        'group_id'=> $group_id,
                    ],
                ]]
        );
        $client->request('POST','https://fcm.googleapis.com/fcm/send',[
                'headers' => [
                    'Authorization' => $this->key,
                    'Content-Type'     => 'application/json',
                ],
                'json' =>[
                    "to" => "/topics/$client_token",
                    "notification" => [
                        "body" => "Приглашения",
                        "title" => "Easy",
                        'order_type'=> "invite",
                        'group_id'=> $group_id,
                        "sound" => "default"
                    ]
                ]]
        );
    }

    public  function List(array $arr){
        $tokens = [];
        $client = \App\Models\Client::find($arr['client_id']);
        if ($arr['type'] == 'service_orders'){
            $tokens = DB::table('drivers')
                ->where('drivers.city_id','=',$client->city_id)

                ->join('cars','cars.driver_id','=','drivers.id')
                ->join('car_transports','car_transports.car_id','=','cars.id')
                ->join('transports','transports.id','=','car_transports.transport_id')
                ->where('transports.id','=',$arr['transport_id'])
                ->pluck('token')
                ->toArray();
        }
        elseif ($arr['type'] == 'shipping_orders'){
            $tokens = DB::table('drivers')

                ->join('cars','cars.driver_id','=','drivers.id')
                ->join('car_transports','car_transports.car_id','=','cars.id')
                ->join('transports','transports.id','=','car_transports.transport_id')
                ->where('drivers.city_id','=',$arr['from_city_id'])
                ->where('transports.id','=',$arr['transport_id'])
                ->pluck('drivers.token')
                ->toArray();
        }
        elseif ($arr['type'] == 'item_orders'){
            $tokens = DB::table('drivers')
                ->join('cars','cars.driver_id','=','drivers.id')
                ->join('car_materials','car_materials.car_id','=','cars.id')
                ->join('materials','materials.id','=','car_materials.material_id')
                ->where('drivers.city_id','=',$client->city_id)
                ->where('materials.id','=',$arr['material_id'])
                ->pluck('drivers.token')
                ->toArray();
        }


        $url = 'https://fcm.googleapis.com/fcm/send';
        $clientGuzzle = new Client();
        foreach ($tokens as $token) {
            $dataA= [
                'headers' => [
                    'Authorization' => $this->key,
                    'Content-Type'     => 'application/json',
                ],
                'json' =>[
                    "to" => "/topics/$token".'a',
                    "data" => [
                        "body" => "Новый заказ!",
                        "title" => "Easy",
                        'order_type'=> mb_substr($arr['type'], 0, -7),
                        'order_id'=> $arr['id']
                    ]
                ]
            ];
            $dataI= [
                'headers' => [
                    'Authorization' => $this->key,
                    'Content-Type'     => 'application/json',
                ],
                'json' =>[
                    "to" => "/topics/$token",
                    "notification" => [
                        "body" => "Новый заказ!",
                        "title" => "Easy",
                        'order_type'=> mb_substr($arr['type'], 0, -7),
                        'order_id'=> $arr['id'],
                        "sound" => "default"
                    ]
                ]
            ];
            $requestA = $clientGuzzle->postAsync( $url, $dataA);
            $requestI = $clientGuzzle->postAsync( $url, $dataI);
        }
        if (count($tokens) != 0){
            $requestA->wait();
            $requestI->wait();
        }


    }


    public function Res(int $a,int $b) : int {


    }
}