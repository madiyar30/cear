const { Model } = require('objection');
const Knex = require('knex');

// Initialize knex.
const knex = Knex({
    client: 'mysql2',
    useNullAsDefault: true,
    connection: {
        host : '127.0.0.1',
        user : 'root',
        password : 'adgjmp96',
        database : 'cear'
    }
});

// Give the knex object to objection.
Model.knex(knex);

class User extends Model {
    static get tableName() {
        return 'users';
    }
}
class TechOrder extends Model {
    static get tableName() {
        return 'tech_orders';
    }
}
class City extends Model {
    static get tableName() {
        return 'cities';
    }
}
class Image extends Model {
    static get tableName() {
        return 'images';
    }
}
class Offer extends Model {
    static get tableName() {
        return 'offers';
    }
}
class Rate extends Model {
    static get tableName() {
        return 'rates';
    }
}


async function AllOffline() {
    await User
        .query()
        .where('online','=',1)
        .update({
            online: 0,
        })
}

async function GetUserByToken(token) {
    const user =  await User.query().where('token',token).first();
    return user;
}
async function GetUserById(id) {
    const user =  await User.query().where('id',id).first();
    return user;
}
async function Geo(data) {
    const user =  await User
        .query()
        .update(
            {lng:data.lng,
            lat:data.lat})
        .where('token',data.token);
}
async function Beside (lng,lat) {

    let users = await  User.raw(
`
SELECT
*,
(
   6371 *
   acos(cos(radians(${lat})) *
   cos(radians(lat)) *
   cos(radians(lng) -
   radians(${lng})) +
   sin(radians(${lat})) *
   sin(radians(lat)))
) AS distance
FROM users
where role <> 'clients'
and role <> 'brigadiers'

HAVING distance < 10
ORDER BY distance LIMIT 0, 20;
`
);
  return users
}
async function Geopositions(city_id) {
    const users = await  User
        .query()
        .select(
            'users.id',
            'users.phone',
            'users.name',
            'users.role',
            'users.email',
            'users.info',
            'users.lng',
            'users.lat',
            'users.avatar',
            'teches.name as tech_name',
            'transports.name as transport_name',
            'transports.icon as transport_icon',
            'marks.name as mark_name'
        )
        .rightJoin('teches', 'users.tech_id', 'teches.id')
        .rightJoin('transports', 'users.transport_id', 'transports.id')
        .rightJoin('marks', 'users.mark_id', 'marks.id')
        .whereIn('role',['drivers','workers'])
        .where('users.city_id','=',city_id)
        .where('users.balance','>',200)
        .where('users.online','=',1)
        .orderBy('id','desc');

    return users
}
async function GetCities() {
    const cities = await  City
        .query();
    return cities
}
async function GetRates(total) {
    const rate = await  Rate
        .query().where("start_price",">",total).where("end_price",total).first()
    return rate
}
async function GetClientTechMyOrders(client_id) {
    const orders = await  TechOrder
        .query()
        .select(
            'tech_orders.*',
            'teches.name as tech_name',
            'transports.name as transport_name',
            'computes.name as compute_name',
            'computes.name as compute_name',
            'commissions.name as commission_name'
        )
        .leftJoin('teches', 'tech_orders.tech_id', 'teches.id')
        .leftJoin('transports', 'tech_orders.transport_id', 'transports.id')
        .leftJoin('computes', 'tech_orders.compute_id', 'computes.id')
        .leftJoin('commissions', 'tech_orders.commission_id', 'commissions.id')
        .where('tech_orders.client_id','=',client_id)
        .orderBy('id','desc');

    for(let i in orders){
        orders[i].images = await Image.query().where('parent_type','tech_orders').where('parent_id',orders[i].id).pluck('path');
    }



    return orders
}
async function GetDriverTechMyOrders(driver_id) {
    const orders = await  TechOrder
        .query()
        .select(
            'tech_orders.*',
            'teches.name as tech_name',
            'transports.name as transport_name',
            'computes.name as compute_name',
            'computes.name as compute_name',
            'commissions.name as commission_name'
        )
        .leftJoin('teches', 'tech_orders.tech_id', 'teches.id')
        .leftJoin('transports', 'tech_orders.transport_id', 'transports.id')
        .leftJoin('computes', 'tech_orders.compute_id', 'computes.id')
        .leftJoin('commissions', 'tech_orders.commission_id', 'commissions.id')
        .where('tech_orders.driver_id','=',driver_id)
        .orderBy('id','desc');

    for(let i in orders){
        orders[i].images = await Image.query().where('parent_type','tech_orders').where('parent_id',orders[i].id).pluck('path');
    }



    return orders
}
async function GetBrigadierTechMyOrders(brigadier_id) {
    const orders = await  TechOrder
        .query()
        .select(
            'tech_orders.*',
            'teches.name as tech_name',
            'transports.name as transport_name',
            'computes.name as compute_name',
            'computes.name as compute_name',
            'commissions.name as commission_name'
        )
        .leftJoin('teches', 'tech_orders.tech_id', 'teches.id')
        .leftJoin('transports', 'tech_orders.transport_id', 'transports.id')
        .leftJoin('computes', 'tech_orders.compute_id', 'computes.id')
        .leftJoin('commissions', 'tech_orders.commission_id', 'commissions.id')
        .where('tech_orders.brigadier_id','=',brigadier_id)
        .orderBy('id','desc');

    for(let i in orders){
        orders[i].images = await Image.query().where('parent_type','tech_orders').where('parent_id',orders[i].id).pluck('path');
        orders[i].worker = await GetUserById(orders[i].driver_id)
    }



    return orders
}
async function GetTechOrder(id) {
    const order = await  TechOrder
        .query()
        .select(
            'tech_orders.*',
            'users.id as client_id',
            'users.phone as phone',
            'users.name as name',
            'users.avatar as avatar',
            'teches.name as tech_name',
            'teches.icon',
            'teches.marker_icon',
            'transports.name as transport_name',
            'computes.name as compute_name',
            'commissions.name as commission_name'
        )
        .leftJoin('users', 'tech_orders.client_id', 'users.id')
        .leftJoin('teches', 'tech_orders.tech_id', 'teches.id')
        .leftJoin('transports', 'tech_orders.transport_id', 'transports.id')
        .leftJoin('computes', 'tech_orders.compute_id', 'computes.id')
        .leftJoin('commissions', 'tech_orders.commission_id', 'commissions.id')
        .where('tech_orders.id','=',id)
        .first();

        order.images =await  Image.query()
            .where('parent_type','tech_orders')
            .where('parent_id',id)
            .pluck('path');

        order.offers =await  Offer.query()
            .select('users.*')
            .innerJoin('users','offers.user_id','users.id')
            .where('order_id',order.id);

    return order
}
async function GetTechOrders(city_id) {
    const orders = await  TechOrder
        .query()
        .select(
            'tech_orders.*',
            'users.id as client_id',
            'users.phone as phone',
            'users.name as name',
            'teches.name as tech_name',
            'teches.icon',
            'teches.marker_icon',
            'transports.name as transport_name',
            'computes.name as compute_name',
            'computes.name as compute_name',
            'commissions.name as commission_name'
        )
        .rightJoin('users', 'tech_orders.client_id', 'users.id')
        .rightJoin('teches', 'tech_orders.tech_id', 'teches.id')
        .rightJoin('transports', 'tech_orders.transport_id', 'transports.id')
        .innerJoin('computes', 'tech_orders.compute_id', 'computes.id')
        .innerJoin('commissions', 'tech_orders.commission_id', 'commissions.id')
        .where('tech_orders.step','=','1')
        .where('users.city_id','=',city_id)
        .orderBy('id','desc');

    for(let i in orders){
        orders[i].images = await Image.query().where('parent_type','tech_orders').where('parent_id',orders[i].id).pluck('path');
    }



    return orders
}
async function UpdateTechOrder(data) {
    await  TechOrder
        .query()
        .update(data)
        .where('id',data.id);

    return await GetTechOrder(data.id)
}
async function UpdateUserToOnline(id) {
    await  User
        .query()
        .update({online:1})
        .where('id',id);
}
async function UpdateUserToOffline(id) {
    await  User
        .query()
        .update({online:0})
        .where('id',id);
}
async function UserSubBalance(id,price) {

    let user =  await User.query().where('id',id).first();
    if  (user){
        await  User
            .query()
            .update({balance:user.balance - price})
            .where('id',id);
    }
}
async function TechOrderCreate(data) {
    let order = await TechOrder.query().insert(data);
    return await GetTechOrder(order.id)
}
async function OfferCreate(data) {

    let offer = await Offer.query().insert(
        {
            order_id:data.id,
            user_id:data.driver_id
        });

    return offer
}
async function Create(firstName){
    // await User.query().insert({firstName:firstName});
}




module.exports.GetUserByToken = GetUserByToken;
module.exports.UserSubBalance = UserSubBalance;
module.exports.GetRates = GetRates;
module.exports.Geo = Geo;
module.exports.Beside = Beside;
module.exports.OfferCreate = OfferCreate;
module.exports.Geopositions = Geopositions;
module.exports.TechOrderCreate = TechOrderCreate;
module.exports.GetCities = GetCities;
module.exports.GetTechOrders = GetTechOrders;
module.exports.GetTechOrder = GetTechOrder;
module.exports.UpdateTechOrder = UpdateTechOrder;
module.exports.GetUserById = GetUserById;
module.exports.GetClientTechMyOrders = GetClientTechMyOrders;
module.exports.GetDriverTechMyOrders = GetDriverTechMyOrders;
module.exports.GetBrigadierTechMyOrders = GetBrigadierTechMyOrders;
module.exports.UpdateUserToOnline = UpdateUserToOnline;
module.exports.UpdateUserToOffline = UpdateUserToOffline;
module.exports.AllOffline = AllOffline;


// BD.Create('firstName');
/*
 BD.all().then(function (res) {
     console.log(res)
 })

(async () => {
    const people = await BD.all();
    console.log(people);
})();
*/
