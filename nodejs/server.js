const Joi = require('joi');
const moment = require('moment');
const BD = require('./bd');
const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const beep = require('beepbeep')
const request = require('request');




let cities = [];
let users =[];


// var log = console.log;
// console.log = function() {
//     log.apply(console, arguments);
//     // Print the stack trace
//     console.trace();
// };


try {
    console.log('--------------------------------------SERVER STARTED---------------------------------------------');
    server.listen(3000);
    BD.GetCities().then(function (res) {
        cities = res;
    });
    BD.AllOffline().then(function () {
        console.log("All Offline Update")
    });

    io.on('connection', function (socket) {
        console.log('socket.id',socket.id);
        socket.on('system',function (data) {
            console.log(data);
            let rules = {
                token:Joi.string().required()
            };
            let error = validate(data,rules,);
            if (error){
                console.log('error');
                io.to(socket.id).emit('system',result(400,'Ошибка данных system',error.message))
            }
            else{
                let user = JSON.parse(data);
                BD.GetUserByToken(user.token).then(function (res) {
                    if (res){
                        socket.user = res;


                        let system = {};
                        system.id = res.id;
                        system.name = res.name;
                        system.phone = res.phone;
                        system.role= res.role;
                        system.lng= res.lng;
                        system.lat= res.lat;
                        system.token = res.token;
                        system.city_id = res.city_id;
                        system.socket_id = socket.id;
                        system.brigadier_id = res.brigadier_id;
                        users.push(system);

                        console.log('System Connect',system);
                        BD.UpdateUserToOnline(system.id).then(function (res) {

                        });

                        io.to(socket.id).emit('system',result(200,'Успешно'))

                        ClientsOrders(res.id);
                        DriversOrders(res.id);
                    }
                    else{
                        io.to(socket.id).emit('system',result(404,'Не найден'))
                    }
                });

            }
        });
        socket.on('geo',function (front) {
            let rules = {
                lng:Joi.required(),
                lat:Joi.required(),
                token:Joi.required()
            };

            let error = validate(front,rules,);
            if (error){
                io.to(socket.id).emit('geo',result(400,'Ошибка данных geo',error.message))
            }
            else{
                let data = JSON.parse(front);
                BD.Geo(data);
                io.to(socket.id).emit('geo',result(200,'Успешно GEO'));


                BD.Beside(data.lng,data.lat).then(function (res) {
                   if (res[0].length !== 0){
                       io.to(socket.id).emit('beside',result(200,'Успешно Beside',res[0]));
                   }
                })
            }
        });
        socket.on('geopositions',function () {
            Geopositions();
        })
        socket.on('tech_orders',function () {
            GetTechOrders();
        })

        socket.on('tech_order_1',function (front) {
            //Создание заказа

            console.log(front)
            var rules =  {
                client_id:Joi.required(),
                address:Joi.required(),
                lng:Joi.required(),
                lat:Joi.required(),
                tech_id:Joi.required(),
                transport_id:Joi.required(),
                compute_id:Joi.required(),
                commission_id:Joi.required(),
                price:Joi.required(),
                comment:Joi.required(),
                step:Joi.required()
            };
            var error = validate(front,rules,);
            if (error){
                io.to(socket.id).emit('tech_order_1',result(400,'Ошибка данных 1',error.message))
            }
            else{
                let data = JSON.parse(front);
                data.created_at = new Date();
                BD.TechOrderCreate(data).then(function (res) {
                    console.log('tech_order_create', res);
                    io.to(socket.id).emit('tech_order_1', result(200, 'Успешно'));
                    BD.GetUserById(data.client_id).then(function (user) {
                        BD.GetTechOrders(user.city_id).then(function (orders) {
                            if (orders.length != 0){
                                io.emit('tech_orders_',+user.city_id,result(200,"Успешно",orders))
                            }
                        });
                    })
                });

                ClientsOrders(data.client_id);
            }
        });
        socket.on('tech_order_2',function (front) {
            //Водитель Принимает
            // var rules = {
            //     id:Joi.required(),
            //     driver_id:Joi.required(),
            //     step:Joi.required(),
            //     brigadier_id:Joi.any().required()
            // };
            // var error = validate(front,rules,);
            // if (error){
            //     io.to(socket.id).emit('tech_order_2',result(400,'Ошибка данных',error.message))
            // }
            // else{
            //
            // }

            let data = JSON.parse(front);
            console.log("step 2",data)
            BD.GetTechOrder(data.id).then(function (order) {
                if  (order){
                    BD.UpdateTechOrder(data).then(function (res) {
                        let client = GetSystemUser(order.client_id);
                        if (client){
                            console.log("client send socket")
                            io.to(client.socket_id).emit('tech_order_2',result(200,'Водитель принял ваш заказ',res));
                        }
                        else{
                            console.log("client not system (step 2)")
                            sendPush(order.client_id,"Водитель принял ваш заказ",order.id,"tech_order_2").then(function () {
                                console.log("step 2 PUSH to Client")
                            })
                        }
                        if  (res.brigadier_id){
                            let user = GetSystemUser(res.driver_id);
                            if (user){
                                io.to(user.socket_id).emit('tech_order_2',result(200,'Новый заказ от Бригадира',res));
                            }else{
                                sendPush(res.driver_id,'Новый заказ от Бригадира',res.id,"tech_order_2").then(function () {
                                    console.log("STEP 2 PUSH to Worker")
                                })
                            }
                        }
                        ClientsOrders(res.client_id);
                        DriversOrders(data.driver_id);
                        io.to(socket.id).emit('tech_order_2',result(200,'Успешно'));
                    });


                }
                else{
                    io.to(socket.id).emit('tech_order_2',result(404,'Не найдено'));
                }
            })
        });
        socket.on('tech_order_3',function (front) {
            //Личный заказ к Водителью
            var rules = {
                driver_id:Joi.required(),
                client_id:Joi.required(),
                address:Joi.required(),
                lng:Joi.required(),
                lat:Joi.required(),
                commission_id:Joi.required(),
                price:Joi.required(),
                comment:Joi.required(),
                step:Joi.required(),
                start_date:Joi.required(),

            };
            var error = validate(front,rules,);
            if (error){
                io.to(socket.id).emit('tech_order_3',result(400,'Ошибка данных ',error.message))
            }
            else{
                let data = JSON.parse(front);

                BD.GetUserById(data.driver_id).then(function (user) {
                    if (user){
                        if (user.brigadier_id) {
                            data.brigadier_id =user.brigadier_id;
                            BD.TechOrderCreate(data).then(function (res) {
                                let brigadier = GetSystemUser(user.brigadier_id);
                                if (brigadier){
                                    res.worker = user;
                                    io.to(brigadier.socket_id).emit('tech_order_3',result(200,'К вам заказ от клиента',res));
                                }else{
                                    sendPush(res.brigadier_id,"К вам заказ от клиента",res.id,"tech_order_3").then(function () {
                                        console.log("STEP 3 SEND PUSH to brigadier")
                                    })
                                }
                                BrigadiersOrders(data.brigadier_id);
                            });
                        }
                        else{
                            BD.TechOrderCreate(data).then(function (res) {
                                io.to(socket.id).emit('tech_order_3', result(200, 'Успешно'));
                                let user = GetSystemUser(data.driver_id);
                                if (user){
                                    io.to(user.socket_id).emit('tech_order_3',result(200,'К вам заказ от клиента',res));
                                }
                                else{
                                    sendPush(res.driver_id,"К вам заказ от клиента",res.id,"tech_order_3").then(function () {
                                        console.log("STEP 3 SEND PUSH to Driver")
                                    })

                                }

                            });

                        }
                    }
                    else{
                        console.log("user not bd (step 3)")
                    }
                });



                ClientsOrders(data.client_id);
                DriversOrders(data.driver_id);

            }
        });
        socket.on('tech_order_4',function (front) {
            //Отказ водителья
            var rules = {
                client_id:Joi.required(),
                driver_id:Joi.required(),
                id:Joi.required(),
                step:Joi.required()
            };
            var error = validate(front,rules,);
            if (error){
                io.to(socket.id).emit('tech_order_4',result(400,'Ошибка данных',error.message))
            }
            else{
                let data = JSON.parse(front);
                BD.GetTechOrder(data.id).then(function (order) {
                    if  (order){
                        BD.UpdateTechOrder(data).then(function (res) {
                            let client = GetSystemUser(order.client_id);
                            if (client){
                                io.to(client.socket_id).emit('tech_order_4',result(4,'Отказ от водителья',res));
                            }
                            else{
                                sendPush(order.client_id,"Отказ от водителья",order.id,"tech_order_4").then(function () {
                                    console.log("SEND PUSH to Client STEP 4")
                                })
                            }
                            io.to(socket.id).emit('tech_order_4',result(200,'Успешно'));
                            ClientsOrders(data.client_id);
                            if  (order.brigadier_id){
                                BrigadiersOrders(order.brigadier_id);
                            }
                            else{
                                DriversOrders(data.driver_id);
                            }

                        });
                    }
                    else{
                        io.to(socket.id).emit('tech_order_4',result(404,'Не найдено'));
                    }
                })
            }
        });
        socket.on('tech_order_5',function (front) {
            //Отказ от клиента``1
            var rules = {
                driver_id:Joi.required(),
                client_id:Joi.required(),
                id:Joi.required(),
                step:Joi.required()
            };
            var error = validate(front,rules,);
            if (error){
                io.to(socket.id).emit('tech_order_5',result(400,'Ошибка данных',error.message))
            }
            else{
                let data = JSON.parse(front);
                BD.GetTechOrder(data.id).then(function (order) {
                    if (order){
                        BD.UpdateTechOrder(data).then(function (res) {
                            let driver = GetSystemUser(order.driver_id);

                            if (driver){
                                io.to(driver.socket_id).emit('tech_order_5',result(200,'Отказ от клиента'));
                            }
                            else{
                                sendPush(order.driver_id,"Отказ от клиента",order.id,"tech_order_5").then(function () {
                                    console.log("SEND PUSH to Driver STEP 5")
                                })
                            }
                            if (order.brigadier_id){
                                let brigadier = GetSystemUser(order.brigadier_id);

                                if (brigadier){
                                    io.to(brigadier.socket_id).emit('tech_order_5',result(200,'Отказ от клиента'));
                                }
                                else{
                                    sendPush(order.brigadier_id,"Отказ от клиента",order.id,"tech_order_5").then(function () {
                                        console.log("SEND PUSH to Driver STEP 5")
                                    })
                                }
                            }
                            io.to(socket.id).emit('tech_order_5',result(200,'Успешно'));
                            ClientsOrders(data.client_id);
                            DriversOrders(data.driver_id);
                            BrigadiersOrders(order.brigadier_id);
                        });
                    } else {
                        io.to(socket.id).emit('tech_order_5',result(404,'Не найдено'));
                    }
                })
            }
        });
        socket.on('tech_order_6',function (front) {
            //Предлагаю цену
            var rules = {
                id:Joi.required(),
                price:Joi.required(),
                step:Joi.required()
            };
            var error = validate(front,rules,);
            if (error){
                io.to(socket.id).emit('tech_order_6',result(400,'Ошибка данных',error.message))
            }
            else{
                let data = JSON.parse(front);
                BD.GetTechOrder(data.id).then(function (order) {
                    if (order) {
                        BD.UpdateTechOrder(data).then(function (res) {
                            let client = GetSystemUser(order.client_id);
                            if (client) {
                                io.to(client.socket_id).emit('tech_order_6', result(200, 'Водитель предлагает свою цену', res));
                            }
                            else {
                                sendPush(order.client_id,"Водитель предлагает свою цену",order.id,"tech_order_6").then(function () {
                                    console.log("SEND PUSH TO CLIENT STEP 6")
                                });
                                console.log("Client not system (step 6)")
                            }
                            ClientsOrders(order.client_id);
                        });
                        io.to(socket.id).emit('tech_order_6', result(200, 'Успешно'));
                    }
                    else {
                        io.to(socket.id).emit('tech_order_6', result(404, 'Не найдено'));
                    }
                });
            }
        });
        socket.on('tech_order_11',function (front) {
            //Принимаю цену
            var rules = {
                id:Joi.required(),
                step:Joi.required()
            };
            var error = validate(front,rules,);
            if (error){
                io.to(socket.id).emit('tech_order_11',result(400,'Ошибка данных',error.message))
            }
            else{
                let data = JSON.parse(front);
                BD.GetTechOrder(data.id).then(function (order) {
                    if (order) {
                        BD.UpdateTechOrder(data).then(function (res) {
                            if  (res.brigadier_id){
                                let brigadier = GetSystemUser(res.brigadier_id);
                                if (brigadier) {
                                    BrigadiersOrders(brigadier.id);
                                    io.to(brigadier.socket_id).emit('tech_order_11', result(200, 'Клиент принял вашу цену', res));
                                }
                                else {
                                    sendPush(order.brigadier_id,"Клиент принял вашу цену",order.id,"tech_order_11").then(function () {
                                        console.log("SEND PUSH TO brigadier STEP 11")
                                    });
                                }
                            }
                            else{
                                let driver = GetSystemUser(order.driver_id);
                                if (driver) {
                                    io.to(driver.socket_id).emit('tech_order_11', result(200, 'Клиент принял вашу цену', res));
                                    DriversOrders(driver.id)
                                }
                                else {
                                    sendPush(order.driver_id,"Клиент принял вашу цену",order.id,"tech_order_11").then(function () {
                                        console.log("SEND PUSH TO driver STEP 11")
                                    });
                                }
                            }
                        });
                        io.to(socket.id).emit('tech_order_11', result(200, 'Успешно'));
                        ClientsOrders(order.client_id);
                    }
                    else {
                        io.to(socket.id).emit('tech_order_11', result(404, 'Не найдено'));
                    }
                });
            }
        });
        socket.on('tech_order_7',function (front) {
            //Водитель закончиль работу
            var rules = {
                id:Joi.required(),
                distance_traveled:Joi.required(),
                flight_count:Joi.required(),
                step:Joi.required()
            };
            var error = validate(front,rules,);
            if (error){
                io.to(socket.id).emit('tech_order_7',result(400,'Ошибка данных',error.message))
            }
            else{
                let data = JSON.parse(front);
                BD.GetTechOrder(data.id).then(function (order) {
                    if (order){
                        data.end_work = new Date();
                        let start = moment(order.start_work);
                        let end = moment(data.end_work);
                        data.hour_work = end.diff(start,'hours');

                        if  (order.compute_id == 1){
                            data.total_amount = order.price * order.hour_work
                        }else if (order.compute_id == 2){
                            data.total_amount = order.price * order.flight_count
                        }else if (order.compute_id == 3){
                            data.total_amount = order.price * order.distance_traveled
                        }
                        BD.GetRates( data.total_amount).then(function (rate) {
                            if  (rate){
                                BD.UserSubBalance(rate.value).then(function (sub) {
                                    console.log("sub balance")
                                })
                            }else{
                                console.log("error")
                            }
                        });

                        BD.UpdateTechOrder(data).then(function (res) {
                            let client = GetSystemUser(order.client_id);
                            if (client){
                                io.to(client.socket_id).emit('tech_order_7',result(200,'Водитель закончил работу',res));
                            }
                            else{
                                console.log("Client not system (step 7)");
                                sendPush(order.client_id,"Водитель закончил работу",order.id,"tech_order_7").then(function () {
                                    console.log("SEND PUSH TO CLIENT STEP 7")
                                });
                            }
                            if  (order.brigadier_id){
                                let brigadier = GetSystemUser(order.brigadier_id);
                                if (brigadier){
                                    io.to(brigadier.socket_id).emit('tech_order_7',result(200,'Водитель закончил работу',res));
                                }
                                else{
                                    sendPush(order.client_id,"Водитель закончил работу",order.id,"tech_order_7").then(function () {
                                        console.log("SEND PUSH TO brigadier STEP 7")
                                    });
                                }

                            }
                            ClientsOrders(order.client_id);
                            BrigadiersOrders(order.brigadier_id);
                            DriversOrders(order.driver_id);
                        });
                        io.to(socket.id).emit('tech_order_7',result(200,'Успешно'));
                    } else{
                        io.to(socket.id).emit('tech_order_7',result(404,'Не найдено'));
                    }
                })

            }
        });
        socket.on('tech_order_8',function (front) {
            var rules = {
                id:Joi.required(),
                compute_id:Joi.required(),
                step:Joi.required()
            };
            var error = validate(front,rules);
            if (error){
                io.to(socket.id).emit('tech_order_8',result(400,'Ошибка данных',error.message))
            }
            else{
                let data = JSON.parse(front);
                BD.GetTechOrder(data.id).then(function (order) {
                    if (order){
                        data.start_work = new Date();
                        BD.UpdateTechOrder(data).then(function (res) {
                            let client = GetSystemUser(order.client_id);

                            if (client){
                                io.to(client.socket_id).emit('tech_order_8',result(200,'Водитель начал работу',res));
                            }
                            else{
                                sendPush(order.client_id,"Водитель начал работу",order.id,"tech_order_8").then(function () {
                                    console.log("SEND PUSH TO client STEP 8")
                                });
                            }
                            if (order.brigadier_id){
                                let brigadier = GetSystemUser(order.brigadier_id);
                                if (brigadier){
                                    io.to(brigadier.socket_id).emit('tech_order_8',result(200,'Водитель начал работу',res));
                                }
                                else{
                                    sendPush(order.client_id,"Водитель начал работу",order.id,"tech_order_8").then(function () {
                                        console.log("SEND PUSH TO brigadier STEP 8")
                                    });
                                }
                            }
                            ClientsOrders(order.client_id);
                            DriversOrders(order.driver_id);
                            BrigadiersOrders(order.brigadier_id);
                        });
                        io.to(socket.id).emit('tech_order_8',result(200,'Успешно'));
                    } else{
                        io.to(socket.id).emit('tech_order_8',result(404,'Не найдено'));
                    }
                })

            }
        });
        socket.on('tech_order_9',function (front) {
            var rules = {
                client_id:Joi.required()
            };
            var error = validate(front,rules);
            if (error){
                io.to(socket.id).emit('tech_order_9',result(400,'Ошибка данных',error.message))
            }
            else{
                let data = JSON.parse(front);
                ClientsOrders(data.client_id);
            }
        });
        socket.on('tech_order_10',function (front) {
            //My Orders Driver
            var rules = {
                driver_id:Joi.required()
            };
            var error = validate(front,rules);
            if (error){
                io.to(socket.id).emit('tech_order_10',result(400,'Ошибка данных',error.message))
            }
            else{
                let data = JSON.parse(front);
                DriversOrders(data.driver_id);

            }
        });
        socket.on('tech_order_12',function (front) {
            var rules = {
                driver_id:Joi.required(),
                id:Joi.required(),
                step:Joi.required(),
            };
            var error = validate(front,rules);
            if (error){
                io.to(socket.id).emit('tech_order_12',result(400,'Ошибка данных',error.message))
            }
            else{
                let data = JSON.parse(front);
                BD.OfferCreate(data).then(function (offer) {
                    io.to(socket.id).emit('tech_order_12',result(200,'Успешно'));
                });

                BD.GetTechOrder(data.id).then(function (order) {
                    if (order){
                        let client = GetSystemUser(order.client_id)
                        if  (client){
                            io.to(client.socket_id).emit('tech_order_12',result(200,'У вас новый отклик',{driver_id:data.driver_id,order:order}));
                        }
                        else{
                            sendPush(order.client_id,"У вас новый отклик",order.id,"tech_order_12").then(function () {
                                console.log("SEND PUSH TO driver STEP 12")
                            });
                        }
                        ClientsOrders(order.client_id);

                    }
                    else{
                        console.log('Order not found')
                    }
                })
            }
        });
        socket.on('tech_order_13',function (front) {
            //Клиент принимает отклиик
            var rules = {
                id:Joi.required(),
                driver_id:Joi.required(),
                step:Joi.required()
            };
            var error = validate(front,rules);
            if (error){
                io.to(socket.id).emit('tech_order_12',result(400,'Ошибка данных',error.message))
            }
            else{
                let data = JSON.parse(front);
                BD.UpdateTechOrder(data).then(function (order) {
                    io.to(socket.id).emit('tech_order_13',result(200,'Успешно'));
                    ClientsOrders(order.client_id);
                    DriversOrders(data.driver_id);

                    let driver = GetSystemUser(order.driver_id)

                    if (driver){
                        io.to(driver.socket_id).emit('tech_order_13',result(200,"Клиент принял",order))
                    }
                    else{
                        sendPush(order.driver_id,"Клиент принял",order.id,"tech_order_13").then(function () {
                            console.log("SEND PUSH TO client STEP 13")
                        });
                    }

                })
            }
        });
        socket.on('tech_order_14',function (front) {
            var rules = {
                brigadier_id:Joi.required()
            };
            var error = validate(front,rules);
            if (error){
                io.to(socket.id).emit('tech_order_10',result(400,'Ошибка данных',error.message))
            }
            else{
                let data = JSON.parse(front);
                BrigadiersOrders(data.brigadier_id);

            }
        });
        socket.on('disconnect', function (data) {
            for( let i=0, len=users.length; i<len; ++i ){
                if(users[i].socket_id == socket.id){
                    console.log('System Disconnect',users[i]);

                    BD.UpdateUserToOffline(users[i].id).then(function (res) {
                    });
                    users.splice(i,1);
                    break;
                }
            }
            console.log('в системе ',users.length ,'пользвателей')
        });
    });
}catch (e) {
    console.log("Ошибка сервера",e.message)
}

 function validate(value,schema) {
  try {
      let data =  JSON.parse(value);
      let result = Joi.validate(data, schema);
      return result.error;
  }catch(e)
    {
        let error = {message:"Parse JSON ERROR "+ e.message};
        console.log('JSON PARSE ERROR');
        return error;
    }
}
 function result(statusCode,message,data) {
    if  (statusCode == 400)console.log(message);
    return JSON.stringify({statusCode:statusCode,message:message,data:data});
}
 function GetSystemUser(user_id) {
    let id = parseInt(user_id);
    for(let user of users){
        if(user.id === id || user.id === id){
            return user;
        }
    }
}
 async function sendPush (user_id,message,id,type) {
    var headers = {
        'Authorization': 'key=AAAA4usaBoQ:APA91bHzPGD_QtRFV_mZLwB8YzJhD_fqBtansaNm3L7hgfVCk350TRgSKJX0pPXHaJOcl8Nhnulk4pP6AzXZ2v5fkEXeGtfg1EHliLEHbnR9V_nfPoa-pNlAspmWX9K4Ew-B0_3eZ2Hs',
        'Content-Type': 'application/json'
    }
// Configure the request
    var options = {
        url: 'https://fcm.googleapis.com/fcm/send',
        method: 'POST',
        headers: headers,
        json: true,
        body: {
            to:`/topics/${user_id}a`,
            data:{
                message:message,
                id:id,
                type:type
            },
            time_to_live : 300
        }

    }
    var options_ios = {
        url: 'https://fcm.googleapis.com/fcm/send',
        method: 'POST',
        headers: headers,
        json: true,
        body: {
            to:`/topics/${user_id}`,
            notification:{
                body:message,
                id:id,
                type:type,
                sound:'default'
            },
            time_to_live : 300
        }

    };
// Start the request
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
        }
    });
    request(options_ios, function (error, response, body) {
        if (!error && response.statusCode == 200) {
        }
    })
}
function ClientsOrders(client_id){
    BD.GetClientTechMyOrders(client_id).then(function (order) {
        let client = GetSystemUser(client_id);
        if (client){
          if (order.length !== 0){
              io.to(client.socket_id).emit('tech_order_9',result(200,'Успешно',order));
          }
        }
    })
}
function DriversOrders(driver_id){
    BD.GetDriverTechMyOrders(driver_id).then(function (order) {
        let driver = GetSystemUser(driver_id);
        if (driver){
            if  (order.length !== 0){
                io.to(driver.socket_id).emit('tech_order_10',result(200,'Успешно',order));
            }
        }
    })
}
function BrigadiersOrders(brigadier_id){
   if (brigadier_id){
       BD.GetBrigadierTechMyOrders(brigadier_id).then(function (order) {
           let brigadier = GetSystemUser(brigadier_id);
           if (brigadier){
               if  (order.length !== 0){
                   io.to(brigadier.socket_id).emit('tech_order_14',result(200,'Успешно',order));
               }
           }
       })
   }
}
 setInterval( function () {
     GetTechOrders()
 },15000);
 setTimeout(function () {
     setInterval(async function () {
         Geopositions()
     },15000);
 },5000);

 async function GetTechOrders() {
     console.log("GetTechOrders");
     for (let city of cities){
         BD.GetTechOrders(city.id).then(function (res) {
             if (res.length != 0){
                 io.emit("tech_orders_"+city.id,result(200,"Успешно",res))
             }
         });
     }
 }

async function Geopositions() {
    console.log("Geopositions");
    for (let city of cities){
        BD.Geopositions(city.id).then(function (res) {
            if (res.length != 0){
                io.emit('geopositions_'+ city.id,result(200,"Успешно geopositions",res))
            }
            else{
                io.emit('geopositions_'+ city.id,result(200,"Успешно geopositions",[]))
            }
        })
    }
}