<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('phone')->unique();
            $table->string('email')->unique();
            $table->string('name')->nullable();
            $table->integer('role')->default(1);
            $table->text('info')->nullable();
            $table->float('lng')->nullable();
            $table->float('lat')->nullable();
            $table->integer('tech_id')->nullable();
            $table->integer('transport_id')->nullable();
            $table->integer('mark_id')->nullable();
            $table->integer('balance')->default(0);
            $table->string('avatar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
