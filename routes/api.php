<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    Route::post('register', 'UserController@Register');
    Route::post('login', 'UserController@Login');
    Route::post('balance/result', 'UserController@AddBalanceResult')->name("AddBalanceResult");
    Route::post('call/order', 'UserController@Call');

    Route::group(['middleware' => ['auth']], function () {
        Route::post('auth', 'UserController@Auth');
        Route::post('balance', 'UserController@AddBalance');
        Route::post('transition', 'UserController@UserTransition');
        Route::post('driver/register', 'UserController@DriverRegister');
        Route::post('brigadier/register', 'UserController@BrigadierRegister');
        Route::post('brigadier/worker/add', 'UserController@WorkerAdd');
        Route::post('brigadier/worker/remove', 'UserController@WorkerRemove');
        Route::post('histories', 'UserController@Histories');
        Route::post('driver/rating/add', 'UserController@RatingAdd');
    }



    );
    Route::prefix('list')->group(function () {
        Route::get('cities','ListController@Cities');
        Route::get('marks','ListController@Marks');
        Route::get('techs','ListController@Techs');
        Route::get('transports','ListController@Transports');
        Route::get('rates','ListController@Rates');
        Route::get('computes','ListController@Сomputes');
        Route::get('commissions','ListController@Commissions');
        Route::get('material_cats','ListController@CatMaterials');
        Route::get('part_cats','ListController@CatParts');
        Route::get('cat_car_orders','ListController@CatCarOrders');
    });

    Route::prefix('material')->group(function () {
        Route::get('company/{id}','MaterialController@GetCompany');

        Route::group(['middleware' => ['auth']], function () {
            Route::get('companies','MaterialController@Companies');
            Route::post('beside','MaterialController@Beside');
            Route::get('search','MaterialController@Search');
            Route::post('order/create','MaterialController@OrderCreate');
            Route::post('order/my','MaterialController@MyOrders');

            Route::post('basket','MaterialController@Basket');
            Route::post('basket/delete','MaterialController@BasketDelete');
            Route::post('basket/all/delete','MaterialController@BasketAllDelete');
            Route::post('basket/list','MaterialController@ListBasket');
            Route::post('rating/add','MaterialController@RatingAdd');

        });
    });

    Route::prefix('part')->group(function () {
        Route::get('shop/{id}','PartController@GetShop');

        Route::group(['middleware' => ['auth']], function () {
            Route::get('shops','PartController@Shops');
            Route::post('beside','PartController@Beside');
            Route::post('order/create','PartController@OrderCreate');
            Route::post('order/my','PartController@MyOrders');

            Route::post('basket','PartController@Basket');
            Route::post('basket/delete','PartController@BasketDelete');
            Route::post('basket/all/delete','PartController@BasketAllDelete');
            Route::post('basket/list','PartController@ListBasket');
            Route::post('rating/add','PartController@RatingAdd');

        });
    });

    Route::prefix('car_order')->group(function () {
        Route::get('get/{id}','CarOrderController@GetCarOrder');
        Route::get('list','CarOrderController@CarOrders');
        Route::group(['middleware' => ['auth']], function () {
            Route::get('my/list','CarOrderController@MyCarOrders');
            Route::post('create','CarOrderController@CreateCarOrder');
            Route::post('review/create','CarOrderController@ReviewsCreate');
        });
    });


    Route::prefix('shipping')->group(function () {

        Route::get('get/{id}','ShippingController@GetShipping');
        Route::get('count','ShippingController@Count');
        Route::get('list','ShippingController@Shippings');
        Route::group(['middleware' => ['auth']], function () {

            Route::post('cargo/create','ShippingController@CreateShippingCargo');
            Route::post('transport/create','ShippingController@CreateShippingTransport');

        });
    });


    Route::fallback(function(){

        $result['statusCode'] = 404;
        $result['message'] = 'api not found';
        $result['result'] = null;

        return response()->json($result, 404);
    });

    Route::get('clear',function (){
        foreach (\App\Models\TechOrder::all() as $item) {
            $item->delete();
        }
        return 1;
    });

